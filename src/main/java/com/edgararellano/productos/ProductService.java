package com.edgararellano.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }
    public ProductoModel save(ProductoModel prod){
        return productoRepository.save(prod);
    }

    public Boolean delete(ProductoModel prod){
        try {
            productoRepository.delete(prod);
            return true;
        } catch (Exception ex){
            return false;
        }
    }

}
