package com.edgararellano.productos;



import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Document(collection= "com/edgararellano/productos")
public class ProductoModel {
    @Id
    @NotNull
    private String id;
    private String nombre;
    private String descriptor;
    private String precio;


    public ProductoModel() {
    }

    public ProductoModel(@NotNull String id, String nombre, String descriptor, String precio) {
        this.id = id;
        this.nombre = nombre;
        this.descriptor = descriptor;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }


}
