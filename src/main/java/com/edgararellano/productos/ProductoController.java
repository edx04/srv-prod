package com.edgararellano.productos;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductoController {
    @Autowired
    ProductService productoService;



    @GetMapping("/productos")
    public List<ProductoModel> getProducto(){
        return productoService.findAll();
    }

    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel prod){
        return productoService.save(prod);
    }

    @PutMapping("/productos")
    public void putProducto(@RequestBody ProductoModel prod){
        productoService.save(prod);
    }

    @DeleteMapping ("/productos")
    public  boolean deleteProductos(@RequestBody ProductoModel prod){
        return  productoService.delete(prod);
    }


}
